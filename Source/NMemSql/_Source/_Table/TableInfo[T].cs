﻿namespace NMemSql
{
    #region Usings
    using System;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Reflection;
    #endregion

    public class TableInfo<T> : TableInfoBase<T>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public TableInfo<T> SetupPrimaryKey(Expression<Func<T, decimal>> predicate)
        {
            if (predicate == null) throw new ArgumentNullException("predicate");
            this.SetupPrimaryKey(ExpressionHelper<T>.GetProperty(predicate));
            return this;
        }

        public TableInfo<T> SetupPrimaryKey(PropertyInfo propertyInfo)
        {
            if (propertyInfo == null) throw new ArgumentNullException("propertyInfo");
            this.PrimaryKeyPropertyInfo = propertyInfo;

            if (!this.Columns.Contains(this.PrimaryKeyPropertyInfo))
            {
                this.Columns.Add(this.PrimaryKeyPropertyInfo);
            }
            return this;
        }

        public TableInfo<T> SetupColumn(PropertyInfo propertyInfo)
        {
            if (propertyInfo == null) { throw new ArgumentNullException("propertyInfo"); }

            if (this.Columns.Contains(propertyInfo))
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Strings.StringColumnXHasBeenAlreadyAdded, propertyInfo.Name));
            }

            this.Columns.Add(propertyInfo);

            return this;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public TableInfo<T> SetupColumn<TValue>(Expression<Func<T, TValue>> predicate)
        {
            if (predicate == null) { throw new ArgumentNullException("predicate"); }

            PropertyInfo propertyInfo = ExpressionHelper<T>.GetProperty(predicate);
            this.SetupColumn(propertyInfo);

            return this;
        }

        public TableInfo<T> SetupCustomEntityCloner(IEntityCloner<T> entityCloner)
        {
            if (entityCloner == null) throw new ArgumentNullException("entityCloner");
            
            this.CustomEntityCloner = entityCloner;
            
            return this;
        }
    }
}
