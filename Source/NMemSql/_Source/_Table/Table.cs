﻿namespace NMemSql
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;

    #endregion

    public class Table<T> : ITransactionElement
    {
        #region Constants
        private const string ConstDefaultNameFormat = "Table_{0}";
        #endregion

        #region Private variables
        private readonly string name;

        private decimal sequence;
        private decimal sequenceIncrement;
        private readonly PropertyInfo primaryKeyPropertyInfo;

        private readonly Dictionary<decimal, T> entities;
        private readonly IList<PropertyInfo> columns;

        private readonly IEntityCloner<T> entityCloner;
        #endregion

        #region Constructors
        public Table(TableInfoBase<T> tableInfo)
        {
            MyValidateType();

            if (tableInfo == null)
            {
                throw new ArgumentNullException("tableInfo");
            }

            if (tableInfo.PrimaryKeyPropertyInfo == null)
            {
                throw new InvalidOperationException(Strings.StringPrimaryKeyCannotBeNull);
            }

            if (tableInfo.PrimaryKeyIncrementValue <= 0)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Strings.StringValueShouldBeGreaterThanX, 0));
            }

            this.name = tableInfo.UniqueName;
            this.sequence = tableInfo.PrimaryKeyInitialValue;
            this.sequenceIncrement = tableInfo.PrimaryKeyIncrementValue;

            this.primaryKeyPropertyInfo = tableInfo.PrimaryKeyPropertyInfo;
            this.entities = new Dictionary<decimal, T>();

            this.columns = new List<PropertyInfo>(tableInfo.Columns);

            if (string.IsNullOrEmpty(this.name))
            {
                this.name = string.Format(CultureInfo.InvariantCulture, ConstDefaultNameFormat, Guid.NewGuid());
            }

            if (tableInfo.CustomEntityCloner != null)
            {
                this.entityCloner = tableInfo.CustomEntityCloner;
            }
            else
            {
                this.entityCloner = new DefaultEntityCloner<T>(this.columns);
            }
        }
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public int Count
        {
            get
            {
                return this.entities.Count;
            }
        }
        #endregion

        #region Methods
        public virtual void Insert(T entity)
        {
            decimal key = this.MyGetPrimaryKeyValue(entity);

            if (key == 0)
            {
                this.sequence = this.sequence + this.sequenceIncrement;
                MySetPrimaryKeyValue(entity, this.sequence);
            }
            else if (this.entities.ContainsKey(key))
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Strings.StringEntityWithProvidedKeyXAlreadyExists, key));
            }

            this.MyAddEntity(entity);
        }

        public virtual void Delete(T entity)
        {
            decimal key = this.MyGetPrimaryKeyValue(entity);
            this.entities.Remove(key);
        }

        public virtual void Update(T entity)
        {
            decimal primaryKey = this.MyGetPrimaryKeyValue(entity);
            T oldEntity = this.entities[primaryKey];

            this.entities.Remove(primaryKey);

            T addedEntity = this.MyAddEntity(entity);
        }

        public virtual T GetByPrimaryKey(decimal key)
        {
            T foundEntity;
            this.entities.TryGetValue(key, out foundEntity);
            if (foundEntity.Equals(default(T)))
            {
                return default(T);
            }
            else
            {
                return this.MyClone(foundEntity);
            }
        }

        public virtual IList<T> GetAll()
        {
            return this.GetAll(null);
        }

        public virtual IList<T> GetAll(Func<T, bool> predicate)
        {
            List<T> queryResult;
            if (predicate == null)
            {
                queryResult = this.entities.Values.ToList();
            }
            else
            {
                queryResult = this.entities.Values.Where(predicate).ToList();
            }

            List<T> result = new List<T>();
            foreach (T loopEntity in queryResult)
            {
                result.Add(this.MyClone(loopEntity));
            }
            return result;
        }
        #endregion

        #region ITransactionElement methods
        object ITransactionElement.CreateMemento()
        {
            TableMemento memento = new TableMemento();
            memento.Sequence = this.sequence;

            foreach (var loopPair in this.entities)
            {
                memento.Entities.Add(loopPair.Key, this.MyClone(loopPair.Value));
            }

            return memento;
        }

        void ITransactionElement.RestoreMemento(object memento)
        {
            TableMemento tableMemento = (TableMemento)memento;

            this.sequence = tableMemento.Sequence;
            this.entities.Clear();

            var mementoEntities = tableMemento.Entities;

            foreach (var loopPair in mementoEntities)
            {
                decimal key = loopPair.Key;
                T entity = (T)loopPair.Value;
                this.entities.Add(key, entity);
            }
        }
        #endregion

        #region My methods
        private T MyClone(T entity)
        {
            return this.entityCloner.Clone(entity);
        }

        private static void MyValidateType()
        {
            Type type = typeof(T);
            List<ConstructorInfo> constructors = type.GetConstructors().ToList();

            bool hasParametlessConstructor = false;

            foreach (ConstructorInfo loopConstructorInfo in constructors)
            {
                int parametersCount = loopConstructorInfo.GetParameters().Count();

                if (parametersCount == 0)
                {
                    hasParametlessConstructor = true;
                }
            }

            if (!hasParametlessConstructor)
            {
                throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, Strings.StringTypeXHasNoParameterlessConstructor, type.Name));
            }
        }

        private void MySetPrimaryKeyValue(T entity, decimal value)
        {
            Type type = this.primaryKeyPropertyInfo.PropertyType;
            var convertedValue = Convert.ChangeType(value, type, CultureInfo.InvariantCulture);
            this.primaryKeyPropertyInfo.SetValue(entity, convertedValue);
        }

        private decimal MyGetPrimaryKeyValue(T entity)
        {
            return Convert.ToDecimal(this.primaryKeyPropertyInfo.GetValue(entity), CultureInfo.InvariantCulture);
        }

        private T MyAddEntity(T entity)
        {
            T clone = this.MyClone(entity);
            decimal primaryKey = this.MyGetPrimaryKeyValue(entity);
            this.entities.Add(primaryKey, clone);
            return clone;
        }
        #endregion
    }
}
