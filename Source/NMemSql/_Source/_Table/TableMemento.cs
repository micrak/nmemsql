﻿namespace NMemSql
{
    #region Usings
    using System.Collections.Generic;
    #endregion

    public class TableMemento
    {
        #region Constructors
        public TableMemento()
        {
            this.Entities = new Dictionary<decimal, object>();
        }
        #endregion

        #region Properties
        public decimal Sequence { get; set; }

        public IDictionary<decimal, object> Entities { get; private set; }
        #endregion
    }
}
