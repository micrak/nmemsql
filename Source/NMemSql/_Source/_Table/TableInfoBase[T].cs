﻿namespace NMemSql
{
    #region Usings
    using System.Collections.Generic;
    using System.Reflection;
    #endregion

    public abstract class TableInfoBase<T>
    {
        #region Constructors
        protected TableInfoBase()
        {
            this.Columns = new List<PropertyInfo>();
            this.PrimaryKeyInitialValue = 0;
            this.PrimaryKeyIncrementValue = 1;
            this.CustomEntityCloner = null;
        }
        #endregion

        #region Properties
        public string UniqueName { get; set; }

        public PropertyInfo PrimaryKeyPropertyInfo { get; protected set; }

        public decimal PrimaryKeyInitialValue { get; set; }

        public decimal PrimaryKeyIncrementValue { get; set; }

        public IList<PropertyInfo> Columns { get; private set; }

        public IEntityCloner<T> CustomEntityCloner { get; protected set; }
        #endregion
    }
}
