﻿namespace NMemSql
{
    #region Usings
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    #endregion

    public static class ExpressionHelper<T>
    {
        #region Methods
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static PropertyInfo GetProperty<TValue>(Expression<Func<T, TValue>> expression)
        {
            try
            {
                Expression body;
                LambdaExpression lambda = expression as LambdaExpression;
                if (lambda == null)
                {
                    body = expression;
                }
                else
                {
                    body = lambda.Body;
                }

                if (body.NodeType == ExpressionType.MemberAccess)
                {
                    MemberExpression memberExpression = body as MemberExpression;
                    return memberExpression.Member as PropertyInfo;
                }
                else if (body.NodeType == ExpressionType.Convert && body is UnaryExpression)
                {
                    UnaryExpression unraryExpression = body as UnaryExpression;
                    MemberExpression memberExpression = unraryExpression.Operand as MemberExpression;
                    return memberExpression.Member as PropertyInfo;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            catch (Exception exception)
            {
                throw new InvalidOperationException(Strings.StringUnrecognizedPrimaryKeyPredicateExpression, exception);
            }
        }
        #endregion
    }
}
