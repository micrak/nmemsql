﻿namespace NMemSql
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    #endregion

    internal static class CloningHelper
    {
        #region Methods
        public static T ReflectionClone<T>(T item, IList<PropertyInfo> properties)
        {
            T newItem = Activator.CreateInstance<T>();
            foreach (PropertyInfo loopPropertyInfo in properties)
            {
                object value = loopPropertyInfo.GetValue(item);
                loopPropertyInfo.SetValue(newItem, value);
            }
            return newItem;
        }
        #endregion
    }
}
