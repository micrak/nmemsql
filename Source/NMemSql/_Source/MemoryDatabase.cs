﻿namespace NMemSql
{
    #region Usings
    using System;
    using System.Collections.Generic;
    #endregion

    public class MemoryDatabase
    {
        #region Private variables
        private readonly IDictionary<string, object> tables;
        private readonly IDictionary<string, TableMemento> tablesMementos;
        private bool isInTransaction;
        #endregion

        #region Constructors
        public MemoryDatabase()
        {
            this.tables = new Dictionary<string, object>();
            this.tablesMementos = new Dictionary<string, TableMemento>();
            this.isInTransaction = false;
        }
        #endregion

        #region Properties
        public virtual bool IsInTransaction
        {
            get
            {
                return this.isInTransaction;
            }
        }
        #endregion

        #region Methods
        protected virtual Table<T> CreateTable<T>(TableInfoBase<T> tableInfo)
        {
            var table = new Table<T>(tableInfo);
            this.tables.Add(table.Name, table);
            return table;
        }

        public virtual void BeginTransaction()
        {
            if (this.isInTransaction)
            {
                throw new InvalidOperationException(Strings.StringTransactionIsAlreadyStarted);
            }

            this.isInTransaction = true;

            foreach (var loopPair in this.tables)
            {
                ITransactionElement transactionElement = loopPair.Value as ITransactionElement;
                string tableName = loopPair.Key;
                object memento = transactionElement.CreateMemento();
                this.tablesMementos[tableName] = memento as TableMemento;
            }
        }

        public virtual void EndTransaction()
        {
            if (!this.isInTransaction)
            {
                throw new InvalidOperationException(Strings.StringTransactionIsNotStarted);
            }

            this.tablesMementos.Clear();
            this.isInTransaction = false;
        }

        public virtual void RollbackTransaction()
        {
            if (!this.isInTransaction)
            {
                throw new InvalidOperationException(Strings.StringTransactionIsNotStarted);
            }

            foreach (var loopPair in this.tablesMementos)
            {
                string tableName = loopPair.Key;
                TableMemento memento = loopPair.Value;

                ITransactionElement transactionElement = this.tables[tableName] as ITransactionElement;
                transactionElement.RestoreMemento(memento);
            }

            this.tablesMementos.Clear();
            this.isInTransaction = false;
        }
        #endregion
    }
}
