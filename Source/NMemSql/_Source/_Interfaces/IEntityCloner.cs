﻿namespace NMemSql
{
    public interface IEntityCloner<T>
    {
        T Clone(T entity);
    }
}
