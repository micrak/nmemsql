﻿namespace NMemSql
{
    internal interface ITransactionElement
    {
        object CreateMemento();
        void RestoreMemento(object memento);
    }
}
