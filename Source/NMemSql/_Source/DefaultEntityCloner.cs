﻿namespace NMemSql
{
    #region Usings
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    #endregion

    public class DefaultEntityCloner<T> : IEntityCloner<T>
    {
        #region Private variables
        private readonly IList<PropertyInfo> properties;
        #endregion

        #region Constructors
        public DefaultEntityCloner(IList<PropertyInfo> properties)
        {
            if (properties == null) throw new ArgumentNullException("properties");
            this.properties = properties;
        }
        #endregion

        #region IEntityCloner<T> methods
        public T Clone(T entity)
        {
            return CloningHelper.ReflectionClone<T>(entity, this.properties);
        }
        #endregion
    }
}
