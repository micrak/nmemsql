﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NMemSql {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("NMemSql.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The column &apos;{0}&apos; has been already added..
        /// </summary>
        internal static string StringColumnXHasBeenAlreadyAdded {
            get {
                return ResourceManager.GetString("StringColumnXHasBeenAlreadyAdded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The entity with provided key &apos;{0}&apos; already exists..
        /// </summary>
        internal static string StringEntityWithProvidedKeyXAlreadyExists {
            get {
                return ResourceManager.GetString("StringEntityWithProvidedKeyXAlreadyExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The primary key cannot be null..
        /// </summary>
        internal static string StringPrimaryKeyCannotBeNull {
            get {
                return ResourceManager.GetString("StringPrimaryKeyCannotBeNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The transaction is already started..
        /// </summary>
        internal static string StringTransactionIsAlreadyStarted {
            get {
                return ResourceManager.GetString("StringTransactionIsAlreadyStarted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The transaction is not started..
        /// </summary>
        internal static string StringTransactionIsNotStarted {
            get {
                return ResourceManager.GetString("StringTransactionIsNotStarted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The type &apos;{0}&apos; has no parameterless constructor..
        /// </summary>
        internal static string StringTypeXHasNoParameterlessConstructor {
            get {
                return ResourceManager.GetString("StringTypeXHasNoParameterlessConstructor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unrecognized primary key predicate expression..
        /// </summary>
        internal static string StringUnrecognizedPrimaryKeyPredicateExpression {
            get {
                return ResourceManager.GetString("StringUnrecognizedPrimaryKeyPredicateExpression", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The value should be greater than &apos;{0}&apos;..
        /// </summary>
        internal static string StringValueShouldBeGreaterThanX {
            get {
                return ResourceManager.GetString("StringValueShouldBeGreaterThanX", resourceCulture);
            }
        }
    }
}
