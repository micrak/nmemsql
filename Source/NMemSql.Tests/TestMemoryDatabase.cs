﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMemSql.Tests
{
    public class ComputerEntity
    {
        public int Id { get; set; }
        public int Cores { get; set; }
        public string Name { get; set; }
        public double MaxTemperature { get; set; }
    }

    public class ComputerEntityWithNoParameterLessConstructor : ComputerEntity
    {
        public ComputerEntityWithNoParameterLessConstructor(string parameter)
        {

        }
    }

    public class ComputerEntityCloner : IEntityCloner<ComputerEntity>
    {
        public ComputerEntity Clone(ComputerEntity entity)
        {
            return new ComputerEntity()
            {
                Id = entity.Id,
                Cores = entity.Cores,
                MaxTemperature = entity.MaxTemperature,
                Name = entity.Name
            };
        }
    }

    public class TestMemoryDatabase : MemoryDatabase
    {
        public TestMemoryDatabase()
        {
            TableInfo<ComputerEntity> tableInfo = new TableInfo<ComputerEntity>()
                .SetupPrimaryKey(g => g.Id)
                .SetupColumn(g => g.Cores)
                .SetupColumn(g => g.MaxTemperature)
                .SetupColumn(g => g.Name);

            this.Computers = this.CreateTable<ComputerEntity>(tableInfo);

            TableInfo<ComputerEntity> tableInfo2 = new TableInfo<ComputerEntity>()
                .SetupPrimaryKey(g => g.Id)
                .SetupColumn(g => g.Cores)
                .SetupColumn(g => g.MaxTemperature)
                .SetupColumn(g => g.Name)
                .SetupCustomEntityCloner(new ComputerEntityCloner());

            this.ComputersWithEntityCloner = this.CreateTable<ComputerEntity>(tableInfo2);
        }

        public void CreatePublicTable<T>(TableInfo<T> tableInfo)
        {
            this.CreateTable<T>(tableInfo);
        }

        public Table<ComputerEntity> Computers { get; private set; }

        public Table<ComputerEntity> ComputersWithEntityCloner { get; private set; }
    }
}
