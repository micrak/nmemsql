﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NMemSql.Tests
{
    [TestFixture]
    public class Tests
    {
        #region Tests
        [Test]
        public void Insert_single_item()
        {
            TestMemoryDatabase database = this.MyCreateDatabase();           

            ComputerEntity computer = new ComputerEntity()
            {
                Id = 0,
                Cores = 4,
                Name = "Dell"
            };

            database.Computers.Insert(computer);

            Assert.AreEqual(1, database.Computers.Count);
        }

        [Test]
        public void Insert_and_delete_one_item()
        {
            TestMemoryDatabase database = this.MyCreateDatabase();
            ComputerEntity computer = new ComputerEntity();
            database.Computers.Insert(computer);
            database.Computers.Delete(computer);
            Assert.AreEqual(0, database.Computers.Count);
        }

        [Test]
        public void Insert_one_item_in_transaction_scope_and_rollback()
        {
            TestMemoryDatabase database = this.MyCreateDatabase();
            var computer1 = new ComputerEntity();
            var computer2 = new ComputerEntity();
            
            database.Computers.Insert(computer2);

            database.BeginTransaction();
            database.Computers.Insert(computer1);
            database.RollbackTransaction();

            Assert.AreEqual(1, database.Computers.Count);
        }

        [Test]
        public void Insert_one_item_and_query_it()
        {
            var database = this.MyCreateDatabase();
            database.Computers.Insert(new ComputerEntity() { Name = "Memintosh" });

            ComputerEntity foundComputer = database.Computers.GetAll(g => g.Name.Equals("Memintosh")).FirstOrDefault();

            Assert.IsNotNull(foundComputer);
            Assert.AreEqual("Memintosh", foundComputer.Name);
            Assert.AreEqual(1, foundComputer.Id);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Insert_duplicated_entity_throws_exception()
        {
            var database = this.MyCreateDatabase();
            ComputerEntity computer = new ComputerEntity() { Id = 9 };
            database.Computers.Insert(computer);
            database.Computers.Insert(computer);
        }

        [Test]
        public void Insert_100_000_entities()
        {
            var database = this.MyCreateDatabase();
            for (int i = 0; i < 100000; i++)
            {
                var computer = new ComputerEntity()
                {
                    Name = "Computer " + i
                };

                database.Computers.Insert(computer);
            }
        }

        [Test]
        public void Insert_and_check_properties()
        {
            var database = this.MyCreateDatabase();

            var newComputer = new ComputerEntity()
            {
                Cores = 9,
                Id = 4,
                MaxTemperature = 4.4,
                Name = "Stephan"
            };

            database.Computers.Insert(newComputer);

            ComputerEntity foundComputer = database.Computers.GetAll(g => g.Id == 4).FirstOrDefault();
            Assert.IsNotNull(foundComputer);
            Assert.AreNotSame(newComputer, foundComputer);
            Assert.AreEqual(9, foundComputer.Cores);
            Assert.AreEqual(4.4, foundComputer.MaxTemperature);
            Assert.AreEqual("Stephan", foundComputer.Name);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Insert_item_with_duplicated_id()
        {
            TestMemoryDatabase database = MyCreateDatabase();
            var computer1 = new ComputerEntity()
            {
                Id = 0
            };

            var computer2 = new ComputerEntity()
            {
                Id = 1
            };

            database.Computers.Insert(computer1);
            database.Computers.Insert(computer2);
        }

        [Test]
        public void Insert_item_and_update()
        {
            TestMemoryDatabase database = MyCreateDatabase();
            var computer = new ComputerEntity();
            database.Computers.Insert(computer);

            computer.Name = "HP";

            database.Computers.Update(computer);

            ComputerEntity updatedComputer = database.Computers.GetAll(g => g.Id == 1).FirstOrDefault();
            Assert.IsNotNull(updatedComputer);
            Assert.AreEqual("HP", updatedComputer.Name);
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Create_table_with_entity_parameterless_constructor_throws_exception()
        {
            TestMemoryDatabase database = new TestMemoryDatabase();

            TableInfo<ComputerEntityWithNoParameterLessConstructor> tableInfo = new TableInfo<ComputerEntityWithNoParameterLessConstructor>();
            tableInfo.SetupPrimaryKey(g => g.Id);

            database.CreatePublicTable<ComputerEntityWithNoParameterLessConstructor>(tableInfo);
        }

        [Test]
        public void Insert_100_000_entities_with_custom_entity_cloner()
        {
            TestMemoryDatabase database = MyCreateDatabase();

            for (int i = 0; i < 100000; i++)
            {
                database.ComputersWithEntityCloner.Insert(new ComputerEntity());
            }
        }
        #endregion

        #region My methods
        private TestMemoryDatabase MyCreateDatabase()
        {
            return new TestMemoryDatabase();
        }
        #endregion
    }
}
